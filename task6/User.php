<?php

namespace task6;

class User
{
    public $name;
    public $age;

    public function setAge($age)
    {
        if ($this->validateAge($age)) {
            $this->age = $age;
        }

    }

    public function addAge($age)
    {
        if ($this->validateAge($age)) {
            $this->age += $age;
        }
    }

    public function subAge($age)
    {
        if ($this->validateAge($age)) {
            $this->age -= $age;
        }
    }

    public function validateAge($age)
    {
        if ($age > 0) {
            return true;
        }
        return false;
    }
}