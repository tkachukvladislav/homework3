<?php

namespace task8;

class Student
{
    public $name;
    public $course;
    private $courseAdministator;

    public function transferToNextCourse()
    {
        if ($this->course < 5) {
            $this->course++;
        }
    }

    public function setCourseAdministartor($name)
    {
        $this->courseAdministator = $name;
    }

    public function getCourseAdministartor()
    {
        return $this->courseAdministator;
    }

    private function isCourseCorrect($course)
    {
        if ($course > 0 && $course < 5) {
            return true;
        }
        return false;
    }
}
