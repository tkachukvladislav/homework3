<?php


namespace task15;


class Cart
{
    public $products = [];

    public function add(Product $product)
    {
        $this->products[] = $product;
    }

    public function remove($name)
    {

        foreach ($this->products as $key => $value) {
            if ($value->name == $name) {
                unset($this->products[$key]);
            }

        }
    }

    public function getTotalCost()
    {
        $sumProd = 0;
        foreach ($this->products as $key) {
            $sumProd += $key->getCost();
        }
        return $sumProd;
    }

    public function getTotalQuantity()
    {
        $sumProd = 0;
        foreach ($this->products as $key) {
            $sumProd += $key->quantity;
        }
    }

    public function getAvgPrice(): int
    {
        $cost = $this->getTotalCost();
        $quant = $this->getTotalQuantity();
        return $cost / $quant;
    }
}