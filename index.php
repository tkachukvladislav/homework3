<?php

use task1\Employee;
use task15\Cart;
use task15\Product;
use task2\Employee as Task2;
use task3\User;
use task4\Employee as Task4;
use task5\Rectangle;
use task6\User as Task6;
use task7\User as Task7;
use task8\Student;
use task9\Employee as Task9;
use task11\Employee as Task11;
use task12\City;
use task13\Employee as Task13;
use task14\User as Task14;
use task16\Employee as Task16Emp;
use task16\Student as Task16Stud;

require_once __DIR__ . '/vendor/autoload.php';
//task1
$emp = new Employee();
$emp->name = "Ivan";
$emp->salary = 1000;
$emp->age = 25;

$empVasya = new Employee();
$empVasya->name = "Vasya";
$empVasya->age = 26;
$empVasya->salary = 2000;

$sumAge = $empVasya->age + $emp->age;
$sumSalary = $empVasya->salary + $emp->salary;

echo "TASK1:This sum age : $sumAge --- This sum salary $sumSalary<br><hr>";
//2
$empTask = new Task2();
$empTask->name = "Vladimir";
$empTask->age = 17;
$empTask->salary = 1000;

$empTask2 = new Task2();
$empTask2->age = 45;
$empTask2->name = "Petr";
$empTask2->salary = 2330;

$sum = $empTask2->getSalary() + $empTask->getSalary();

echo "TASK2 : getSum Employee : $sum<br><hr>";

//task3
$usr = new User();
$usr->name = "Kolya";
$usr->age = 25;
$usr->setAge(17);
$usr->setAge(30);

echo "TASK3: Nick age: $usr->age<br><hr>";

//task4
$empl = new Task4();
$empl->name = "Mark";
$empl->salary = 999;
$empl->doubleSalary();

echo "TASK4: doubleSalary--- $empl->salary<br><hr>";

//task5
$rect = new Rectangle();
$rect->height = 10;
$rect->width = 3;
$perimetr = $rect->getPerimeter();
$square = $rect->getSquare();

echo "TASK5 : square = $square ,perimetr = $perimetr<br><hr>";

//task6
$user = new Task6();
$user->setAge(20);
$user->addAge(15);
$user->subAge(5);

echo "TASK6: new Age :$user->age<br><hr>";

//task7
$userTask = new Task7();
$userTask->name = "David";
$userTask->age = 10;
$userTask->setAge(3);
$userTask->addAge(7);
$userTask->subAge(1);

echo "TASK7: new Age :$userTask->age<br><hr>";

//task8
$student = new Student();
$student->name = "ADolf";
$student->course = 1;
$student->setCourseAdministartor("Eva");
$admin = $student->getCourseAdministartor();
$student->transferToNextCourse();

echo "TASK7 : Course student : $student->course , Admin course $admin<br><hr>";

//task9
$task9 = new Task9();
$task9->setName("Harry");
$task9->setAge(18);
$task9->setSalary(1200);

$name = $task9->getName();
$age = $task9->getAge();
$salary = $task9->getSalary();

echo "TASK9: $name , $age ,$salary<br><hr>";

//task11
$task11 = new Task11("Vasya", 25, 1000);
$task11Last = new Task11("Petya", 30, 2000);
$sumSalaryTask11 = $task11->salary + $task11Last->salary;

echo "TASK11: sumSalary=$sumSalaryTask11<br><hr>";

//task12
$city = new City();
$city->name = "Kiev";
$city->population = 2964101;
$city->foundation = 889;
$cityName = $city->name;
$cityFoundation = $city->foundation;
$cityPopulation = $city->population;

$props = [];
$props[] = $cityName;
$props[] = $cityFoundation;
$props[] = $cityPopulation;
echo "TASK12 :";
foreach ($props as $key) {
    echo "$key ";
}
echo "<br><hr>";

//task13
$methods = ['method1' => 'getName', 'method2' => 'getAge'];
$empCall = new Task13();
$empCall->name = "Call";
$empCall->age = 30;

$ageCall = $methods['method2'];
$nameCall = $methods['method1'];

echo "TASK13: ";
echo $empCall->$ageCall();
echo $empCall->$nameCall();
echo "<br><hr>";

//task14
$task14 = new Task14();
$task14->add("name", "IVan");
$task14->add("surname", "Ivanov");
$task14->add("patronymic", "Ivanovich");

echo "TASK14 :";
echo $task14->getFullName();
echo "<br><hr>";

//task15
$apple = new Product("Apple", 25, 2);
$chery = new Product("Chery", 5, 1);
$cart = new Cart();
$cart->add($apple);
$cart->add($chery);

//task16
$arr [] = new Task16Emp("Diggi", 450);
$arr [] = new Task16Emp("Diego", 500);
$arr [] = new Task16Emp("Johan", 650);
$arr [] = new Task16Stud("Mike", 250);
$arr [] = new Task16Stud("John", 250);
$arr [] = new Task16Stud("Roby", 250);
$sumSalaryTask16 = 0;
$sumScholarship = 0;
foreach ($arr as $key) {
    if ($key instanceof Task16Emp) {
        echo $key->name;
    }
}
foreach ($arr as $key) {
    if ($key instanceof Task16Stud) {
        echo $key->name;
    }
}
foreach ($arr as $key) {

    $sum = 0;
    if ($key instanceof Task16Emp) {
        $sumSalaryTask16 += $key->salary;
    } else {
        $sumScholarship += $key->scholarship;
    }

}

echo "TASK16: sumScholarship = $sumScholarship , sumSalary = $sumSalaryTask16";
