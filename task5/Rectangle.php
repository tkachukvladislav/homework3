<?php

namespace task5;

class Rectangle
{
    public $width;
    public $height;

    public function getSquare()
    {
        return $this->height * $this->width;
    }

    public function getPerimeter()
    {
        return ($this->width + $this->height) * 2;
    }
}