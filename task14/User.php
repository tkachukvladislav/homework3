<?php


namespace task14;


class User
{
    private $surname;
    private $name;
    private $patronymic;

    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    public function add($add, $val)
    {
        $this->__set($add, $val);
    }

    public function getFullName()
    {
        $fullName = $this->surname[0];
        $fullName .= $this->name[0];
        $fullName .= $this->patronymic[0];
        return $fullName;
    }
}