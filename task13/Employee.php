<?php


namespace task13;


class Employee
{
    public $name;
    public $age;
    public $salary;

    public function getName()
    {
        return $this->name;
    }

    public function getAge()
    {
        return $this->age;
    }

    public function getSalary()
    {
        return $this->salary;
    }

    public function __call($name, $arguments)
    {
        return $this->name;
    }

    public function checkAge()
    {
        if ($this->age < 18) {
            return false;
        } else {
            return true;
        }
    }
}