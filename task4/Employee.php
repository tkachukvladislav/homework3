<?php

namespace task4;

class Employee
{
    public $name;
    public $salary;

    public function doubleSalary()
    {
        $this->salary *= 2;
    }
}