<?php

namespace task9;

use function Sodium\randombytes_uniform;

class Employee
{
    private $name;
    private $age;
    private $salary;

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setAge($age)
    {
        if ($this->isAgeCorrect($age)) {
            $this->age = $age;
        }
    }

    public function getAge()
    {
        return $this->age;
    }

    public function setSalary($salary)
    {
        $this->salary = $salary;
    }

    public function getSalary()
    {
        return $this->salary . "$";
    }

    private function isAgeCorrect($age)
    {
        if ($age > 0 && $age < 100) {
            return true;
        }
        return false;
    }
}